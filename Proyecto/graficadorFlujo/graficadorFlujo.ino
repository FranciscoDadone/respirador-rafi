#include <TimerOne.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 4

#define PIN_LED 6

Adafruit_SSD1306 display(OLED_RESET);

byte count;
byte sensorArray[128];
byte drawHeight;

/*following functions controls scrolling direction (left/right) and drawing mode (dot/filled)
  These commands are NOT case sensitive, code understands in both capitals and non-capitals of these commands to make it more user friendly.
*/
char filled = 'F'; //decide either filled or dot display (D=dot, any else filled)
char drawDirection = 'R'; //decide drawing direction, from right or from left (L=from left to right, any else from right to left)
char slope = 'W'; //slope colour of filled mode white or black slope (W=white, any else black. Well, white is blue in this dispay but you get the point)


// Variables para el flujo
uint16_t flujo = 0;
long flujoIntegral = 0;
uint16_t cantidadMuestras = 0;

struct Tiempos {
  uint8_t graficar;
  uint16_t caluloIntegral;
} volatile tiempos;


ISR(TIMER0_COMPA_vect){
  if(tiempos.graficar) tiempos.graficar--;
  if(tiempos.caluloIntegral) tiempos.caluloIntegral--;

  //digitalWrite(PIN_LED, !digitalRead(PIN_LED));
}

void confTimer0(){
  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR2A register to 0
  TCCR0B = 0;// same for TCCR2B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 2khz increments
  //OCR0A = 249;//124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  OCR0A = 175;
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for 64 prescaler
  TCCR0B |= (1 << CS02) | (0 << CS01) | (1 << CS00);
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);  
}

void setup() {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)

  for (count = 0; count <= 128; count++) { //zero all elements
    sensorArray[count] = 0;
  }

  Timer1.initialize(10000);  // Periodo en us
  Timer1.attachInterrupt(medirFlujo);
  
  confTimer0();
}

void medirFlujo() {
  drawHeight = map(analogRead(A1), 0, 1023, 0, 50 );
  sensorArray[0] = drawHeight;
  for (count = 128; count >= 2; count--){ // count down from 160 to 2
    sensorArray[count - 1] = sensorArray[count - 2];
  }
}

void loop(){
  graficar();
  calculoIntegral();
}

void calculoIntegral(){
  if(tiempos.caluloIntegral) return;
  tiempos.caluloIntegral = 100;
  
  flujoIntegral = 0;
  int i;
  for(i=0; i<100; i++){
    flujoIntegral += sensorArray[i];
  }
}

void graficar() {
  if(tiempos.graficar) return;
  tiempos.graficar = 5;

  for (count = 1; count <= 128; count++ ){
    if (filled == 'D' || filled == 'd') {
      if (drawDirection == 'L' || drawDirection == 'l') {
        display.drawPixel(count, 50 - sensorArray[count - 1], WHITE);
      } else { //else, draw dots from right to left
        display.drawPixel(128 - count, 50 - sensorArray[count - 1], WHITE);
      }
    } else {
      if (drawDirection == 'L' || drawDirection == 'l')
      {
        if (slope == 'W' || slope == 'w') {
          display.drawLine(count, 50, count, 50 - sensorArray[count - 1], WHITE);
        } else {
          display.drawLine(count, 1, count, 50 - sensorArray[count - 1], WHITE);

        }
      } else {
        if (slope == 'W' || slope == 'w'){
          display.drawLine(128 - count, 50, 128 - count, 50 - sensorArray[count - 1], WHITE);
        } else {
          display.drawLine(128 - count, 1, 128 - count, 50 - sensorArray[count - 1], WHITE);
        }
      }
    }
  }

  drawAxises();
  display.display();  //needed before anything is displayed
  display.clearDisplay(); //clear before new drawing
}

void drawAxises()  //separate to keep stuff neater. This controls ONLY drawing background!
{
  display.setCursor(1, 56);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.print(flujoIntegral);

/*
  display.drawLine(0, 0, 0, 32, WHITE);
  display.drawLine(80, 0, 80, 32, WHITE);

  for (count = 0; count < 40; count += 10)
  {
    display.drawLine(80, count, 75, count, WHITE);
    display.drawLine(0, count, 5, count, WHITE);
  }

  for (count = 10; count < 80; count += 10)
  {
    display.drawPixel(count, 0 , WHITE);
    display.drawPixel(count, 31 , WHITE);
  }
  */
}
