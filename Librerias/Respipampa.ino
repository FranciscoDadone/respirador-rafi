//#include <TimerOne.h>
#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h>
#include "Adafruit_SSD1306.h"

Adafruit_SSD1306 display(-1);
#define OLED_ADDR   0x3C

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

//TIEMPOS DE ON Y DE OFF EN MS
#define T_MS
//#define T_S

#define PARADO              0
#define CICLO               1
#define CICLO_UNICO         2
#define STOP_CICLO_UNICO    3
#define CONFIGURACION       9

// Con pulsadores BLANCOS

#define BTN_MENOS     10
#define BTN_MAS       12
#define BTN_MENU      11

//////////////////////////////
#define RELE1     7 // PD7
#define RELE2     6 // PD6
#define RELE3     5 // PD5
#define RELE4     4 // PD4

#define E1        0 // PD0//4
#define E2        1 // PD1//3
#define E3        2 // PD2
#define E4        3 // PD3

#define E1_PORT   digitalRead(E1)
#define E2_PORT   digitalRead(E2)
#define E3_PORT   digitalRead(E3)
#define E4_PORT   digitalRead(E4)

#define RELE1_ON  digitalWrite(RELE1,HIGH)
#define RELE2_ON  digitalWrite(RELE2,HIGH)
#define RELE3_ON  digitalWrite(RELE3,HIGH)
#define RELE4_ON  digitalWrite(RELE4,HIGH)
#define RELE1_OFF digitalWrite(RELE1,LOW)
#define RELE2_OFF digitalWrite(RELE2,LOW)
#define RELE3_OFF digitalWrite(RELE3,LOW)
#define RELE4_OFF digitalWrite(RELE4,LOW)

#define RELE1_LAT digitalRead(RELE1)
#define RELE2_LAT digitalRead(RELE2)
#define RELE3_LAT digitalRead(RELE3)
#define RELE4_LAT digitalRead(RELE4)

#define BTN_MENOS_PORT  !digitalRead(BTN_MENOS) //con botones blancos --> !
#define BTN_MAS_PORT    !digitalRead(BTN_MAS)
#define BTN_MENU_PORT   !digitalRead(BTN_MENU)

#define CERRAR_RELE                 0
#define ABRIR_RELE                  1

#define MENU_INDICE_MAX 5
#define MENU_INDICE_CFG_RELES (MENU_INDICE_MAX-2)
#define TMAX_INICIO 60

#define TIEMPO_RUIDO_PULSADOR 200u
#define TIEMPO_PANTALLA 100u
#define TMR_SEGUNDOS 1000
#define TMR_MS 100

#define T_ON  0
#define T_OFF 1

void actualizarLineas(void);
void pulsadores(void);
void controlLed(void);
void actualizarPantalla(void);
void grabarEEMPROM(void);
void leerEEMPROM(void);
void blink_led(void);

char buff_oled[30];
uint16_t tiempo_espera = 0, tiempo_espera_ms = 0;
uint16_t repeticiones = 0, repeticiones_cont = 0;
uint8_t rele = 0, ae1 = 0, ae2 = 0, ae3 = 0, ae4 = 0;
uint8_t estadoMain = PARADO;
int8_t indice = 0, finish_ciclo = 1;
uint16_t segundos = TMR_SEGUNDOS;
uint16_t msegundos = TMR_MS;
uint8_t vartiti = 1, vengo_de_cfg = 0;
uint8_t hab[4];
int8_t status_proc = 0, num_rele = 0, forzado = 0, unavez = 0, muestra_msj_disp = 0;

struct Tiempos
{
  uint16_t led;
  uint8_t titi;
  uint8_t rebotePulsador;
  uint8_t actualizarPantalla;
} tiempos;

ISR(TIMER0_COMPA_vect)
{
  if (tiempos.led) tiempos.led--;
  if (tiempos.rebotePulsador)tiempos.rebotePulsador--;
  if (tiempos.actualizarPantalla) tiempos.actualizarPantalla--;
  if (tiempos.titi) tiempos.titi--;
}

void setup()
{
  pinMode(E1, INPUT);
  pinMode(E2, INPUT);
  pinMode(E3, INPUT);
  pinMode(E4, INPUT);

  pinMode(BTN_MENOS, INPUT_PULLUP);
  pinMode(BTN_MAS, INPUT_PULLUP);
  pinMode(BTN_MENU, INPUT_PULLUP);

  pinMode(RELE1, OUTPUT);
  pinMode(RELE2, OUTPUT);
  pinMode(RELE3, OUTPUT);
  pinMode(RELE4, OUTPUT);

  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR2A register to 0
  TCCR0B = 0;// same for TCCR2B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 2khz increments
  OCR0A = 249;//124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for 64 prescaler
  TCCR0B |= (1 << CS01) | (1 << CS00);
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);

  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();

  //leerEEMPROM();
}

void proceso(void)
{
  switch (status_proc)
  {
    case 0:
      break;
  }
}

void loop()
{
  proceso();
  pulsadores();
  actualizarPantalla();

}

void lcd_out(uint8_t col, uint8_t linea)
{
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(col * 10, linea * 9);
  display.print(buff_oled);
}
void actualizarLineas()
{
  display.clearDisplay();

  if (estadoMain == PARADO)
    sprintf(buff_oled, "STOP     R1 R2 R3 R4 ");
  else
    sprintf(buff_oled, "FUNC     R1 R2 R3 R4 ");

  lcd_out(0, 0);

  sprintf(buff_oled, "SALIDA   %d  %d  %d  %d", RELE1_LAT, RELE2_LAT, RELE3_LAT, RELE4_LAT );
  lcd_out(0, 1);
  sprintf(buff_oled, "HABILI   %d  %d  %d  %d", 1, 2, 3, 4);
  lcd_out(0, 2);

  sprintf(buff_oled, "TIEMPO   %d ms  ", tiempo_espera_ms * 100);
  
  lcd_out(0, 3);

  sprintf(buff_oled, ">esperando RST en E4< ");

  lcd_out(0, 5);

  sprintf(buff_oled, "             ARBIT");

  lcd_out(0, 6);

  display.display();
}

void titila_cfg(void)
{
  if (tiempos.titi) return;
  tiempos.titi = 10;

  vartiti = !vartiti;

}

void actualizarLineas2()
{
  titila_cfg();
  display.clearDisplay();

  if (vartiti)
    sprintf(buff_oled, "CONFIG   R1 R2 R3 R4 ");
  else
  {
    if (indice < 2) sprintf(buff_oled, "CONFIG               ");
    else
      switch (rele)
      {
        case 0:
          sprintf(buff_oled, "CONFIG      R2 R3 R4 ");
          break;
        case 1:
          sprintf(buff_oled, "CONFIG   R1    R3 R4 ");
          break;
        case 2:
          sprintf(buff_oled, "CONFIG   R1 R2    R4 ");
          break;
        case 3:
          sprintf(buff_oled, "CONFIG   R1 R2 R3    ");
          break;
      }
  }
  lcd_out(0, 0);
  sprintf(buff_oled, "T. INI    %d      ", 2);
  lcd_out(0, 1);
  sprintf(buff_oled, "T. C-C     ");
  lcd_out(0, 2);
  sprintf(buff_oled, "REPETI       %d  ", 2);
  lcd_out(0, 3);
  sprintf(buff_oled, "HABILI   %d  %d  %d  %d", 1, 2, 3, 4);
  lcd_out(0, 4);
  sprintf(buff_oled, "T. ON    %04dms ", 1);
  lcd_out(0, 5);
  sprintf(buff_oled, "T. OFF   %04dms ", 1);
  lcd_out(0, 6);

  sprintf(buff_oled, "->");
  lcd_out(4, indice + 1);

  display.display();
}

void actualizarPantalla()
{
  if (tiempos.actualizarPantalla) return;
  tiempos.actualizarPantalla = TIEMPO_PANTALLA;

  if (estadoMain == CONFIGURACION )
  {
    actualizarLineas2();
  }
  else
    actualizarLineas();
}

void pulsadores()
{
  if (tiempos.rebotePulsador) return;
  if (BTN_MENOS_PORT == 1 && BTN_MAS_PORT == 1 && BTN_MENU_PORT == 1) return;

  if (estadoMain != CONFIGURACION) return;


  if (BTN_MENU_PORT == 0)
  {
    //CODIGO

    while (BTN_MENU_PORT == 0);
    tiempos.rebotePulsador = TIEMPO_RUIDO_PULSADOR;
  }

  if (BTN_MAS_PORT == 0)
  {
    //CODIGO

    tiempos.rebotePulsador = TIEMPO_RUIDO_PULSADOR;

    //grabarEEMPROM();
  }//

  if (BTN_MENOS_PORT == 0)
  {
    tiempos.rebotePulsador = TIEMPO_RUIDO_PULSADOR;
    //  grabarEEMPROM();
  }
  tiempos.actualizarPantalla = TIEMPO_PANTALLA;
}

void grabarEEMPROM()
{
  /* uint8_t aux1, aux2;

    aux1 = (uint8_t) tiempos.entre_ciclos & 0xFF;
    aux2 = (uint8_t) ((tiempos.entre_ciclos >> 8) & 0xFF);

    EEPROM.write(0, aux1);
    EEPROM.write(1, aux2);

    aux1 = (uint8_t) repeticiones & 0xFF;
    aux2 = (uint8_t) ((repeticiones >> 8) & 0xFF);

    EEPROM.write(2, aux1);
    EEPROM.write(3, aux2);
  */

}

void leerEEMPROM()
{ /*
     uint8_t j=0,k=0;

     tiempos.entre_ciclos= EEPROM.read(0) | EEPROM.read(1) << 8;
     if(tiempos.entre_ciclos>65000)tiempos.entre_ciclos=0;

     repeticiones= EEPROM.read(2) | EEPROM.read(3) << 8;
     repeticiones_cont=repeticiones;

  */
}
