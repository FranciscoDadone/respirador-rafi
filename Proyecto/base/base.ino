#include <TimerOne.h>
#include <Wire.h>
#include "Adafruit_SSD1306.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define PIN_ENABLE 4
#define PIN_STEP 5
#define PIN_DIR 6
#define PIN_FIN_CARRERA 3
#define PIN_PRESION_MAX 7
#define PIN_ELECTROVALVULA 8

#define PIN_POTE_TIEMPO A0
#define PIN_POTE_PRESION A6

#define ASPIRAR LOW
#define EXALAR HIGH

Adafruit_SSD1306 display(-1);
#define OLED_ADDR   0x3C

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

//TIEMPOS DE ON Y DE OFF EN MS
#define T_MS
//#define T_S

#define PARADO              0
#define CICLO               1
#define CICLO_UNICO         2
#define STOP_CICLO_UNICO    3
#define CONFIGURACION       9

// Con pulsadores BLANCOS

#define BTN_MENOS     10
#define BTN_MAS       12
#define BTN_MENU      11

//////////////////////////////
#define BTN_MENOS_PORT  !digitalRead(BTN_MENOS) //con botones blancos --> !
#define BTN_MAS_PORT    !digitalRead(BTN_MAS)
#define BTN_MENU_PORT   !digitalRead(BTN_MENU)

#define TIEMPO_RUIDO_PULSADOR 200u
#define TIEMPO_PANTALLA 100u
#define TMR_SEGUNDOS 1000
#define TMR_MS 100

#define T_ON  0
#define T_OFF 1

// Define para el sensor de presion
#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10
#define SEALEVELPRESSURE_HPA (1013.25)

// Variable para el sensor de presion
Adafruit_BME280 bme; // I2C

char buff_oled[30];
float presion;
byte sensorArray[128];
uint16_t paso;
int estadoMaquina=0;

uint16_t pasosADar;
uint16_t pasosDados;

struct Tiempos
{
  uint16_t led;
  uint8_t mostarDato;
  uint8_t graficar;
  uint16_t motorParado;
  uint8_t usuario;
  uint16_t leerPresion;
  uint16_t imprimirPresion;
} tiempos;


ISR(TIMER0_COMPA_vect)
{
  if(tiempos.led) tiempos.led--;
  if(tiempos.mostarDato) tiempos.mostarDato--;
  if(tiempos.graficar) tiempos.graficar--; 
  if(tiempos.motorParado) tiempos.motorParado--;
  if(tiempos.usuario) tiempos.usuario--;
  if(tiempos.leerPresion) tiempos.leerPresion--;
  if(tiempos.imprimirPresion) tiempos.imprimirPresion--;
}

void confTimer0(){
  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR2A register to 0
  TCCR0B = 0;// same for TCCR2B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 2khz increments
  //OCR0A = 249;//124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  OCR0A = 175;
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for 64 prescaler
  TCCR0B |= (1 << CS02) | (0 << CS01) | (1 << CS00);
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);  
}

void confDisplay(){
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();

  display.clearDisplay();
  sprintf(buff_oled, "Init");
  lcd_out(1, 1);
  display.display();

  if (!bme.begin(0x76, &Wire)) {
    display.clearDisplay();
    sprintf(buff_oled, "Error en el sensor");
    lcd_out(1, 2);
    display.display();
    while(1);  
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println("Init");
  
  pinMode(PIN_ENABLE, OUTPUT);
  pinMode(BTN_MENOS, INPUT_PULLUP);
  pinMode(BTN_MAS, INPUT_PULLUP);
  pinMode(BTN_MENU, INPUT_PULLUP);
  pinMode(PIN_POTE_TIEMPO, INPUT);
  pinMode(PIN_DIR, OUTPUT);
  pinMode(PIN_STEP, OUTPUT);
  pinMode(PIN_FIN_CARRERA, INPUT);
  pinMode(PIN_POTE_PRESION, INPUT);
  pinMode(PIN_PRESION_MAX, INPUT_PULLUP);
  pinMode(PIN_ELECTROVALVULA, OUTPUT);

  digitalWrite(PIN_ELECTROVALVULA, HIGH);
  
  confTimer0();
  //confDisplay();
  
  Timer1.initialize(3000);  // Periodo en us
  Timer1.attachInterrupt(stepInterrupt);

  llevarElMotorAlHome();
}

void llevarElMotorAlHome(){
  if(digitalRead(PIN_FIN_CARRERA) == LOW){
    digitalWrite(PIN_DIR, EXALAR);
    digitalWrite(PIN_ENABLE, LOW);
    Serial.println("Yendo al home...");
    while(digitalRead(PIN_FIN_CARRERA) == LOW){
      paso = 100;
    }
  }
  paso = 0;
  digitalWrite(PIN_ENABLE, HIGH);
}

void setVariables(){
  if(tiempos.usuario) return;
  tiempos.usuario = 10;
  float pote = analogRead(PIN_POTE_TIEMPO); 
  pote = (6000 + (8.7976*pote));
  Timer1.setPeriod((unsigned long)pote);

  uint16_t presionMax = analogRead(PIN_POTE_PRESION);
  pasosADar = 50 + presionMax / 5;
  if(pasosADar%2) pasosADar++;
}

void loop()
{
  setVariables();
  //graficar(); 
  //imprimirPresion();
  //leerPresion();
  controlMotor(); 
}

void leerPresion(){
  if(tiempos.leerPresion) return;
  tiempos.leerPresion = 1;
  
  presion = bme.readPressure() / 100.0F;
}

void controlMotor(){
  switch(estadoMaquina){
    case 0:
      Serial.println("Estado = 0");
      digitalWrite(PIN_ENABLE, LOW);
      digitalWrite(PIN_DIR, ASPIRAR);
      pasosDados = 0;
      paso = pasosADar;
      estadoMaquina++;
      break;
    
    case 1:
      if(!digitalRead(PIN_PRESION_MAX)){
        pasosDados = paso;
        paso = 0;  
      } else{
        if(pasosDados){
          paso = pasosDados;
          pasosDados = 0;
        } else {
          if(paso) break;
          Serial.println("Estado = 1");
          tiempos.motorParado = 100;
          estadoMaquina++;
        }
      }
      break;    

    case 2:
      if(tiempos.motorParado) break;
      Serial.println("Estado = 2");
      estadoMaquina++;
      paso = pasosADar+20;
      digitalWrite(PIN_DIR, EXALAR);
      digitalWrite(PIN_ELECTROVALVULA, LOW);
      break;

    case 3:
      if(digitalRead(PIN_FIN_CARRERA) == HIGH ){
        Serial.println("Encontro el home");
        digitalWrite(PIN_ENABLE, HIGH);
        tiempos.motorParado = 100;
        paso = 0;
        estadoMaquina++;
      } else if(!paso){
          Serial.println("No encontro el home");
          digitalWrite(PIN_ENABLE, HIGH);
          tiempos.motorParado = 100;
          estadoMaquina++;  
        }
      break;
    
    case 4:
      if(tiempos.motorParado) break;
      digitalWrite(PIN_ELECTROVALVULA, HIGH);
      estadoMaquina = 0;
      break;
        
    default:
      break;
    }
}

void stepInterrupt(void)
{
 if(paso){
  paso--;
  digitalWrite(PIN_STEP, !digitalRead(PIN_STEP));
 } else {
  digitalWrite(PIN_STEP, LOW);
 }
}

void lcd_out(uint8_t col, uint8_t linea)
{
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(col * 10, linea * 9);
  display.print(buff_oled);
}

void graficar(){
  byte drawHeight;
  int count;
  
  if(tiempos.graficar) return;
  tiempos.graficar = 50;

  //imprimirPresion();
  drawHeight = map(presion-990, 0, 100, 0, 32 );
  //drawHeight = map(analogRead(A0), 0, 1023, 0, 32 );
  
  sensorArray[0] = drawHeight;
  //for (count = 1; count <= 128; count++ )
  //  display.drawLine(128 - count, 32, 128 - count, 32 - sensorArray[count - 1], WHITE);
   
  //for (count = 160; count >= 2; count--) // count down from 160 to 2
  //  sensorArray[count - 1] = sensorArray[count - 2];


  display.ssd1306_command(0x27);
  display.ssd1306_command(0x00);
  display.ssd1306_command(0x00); // Start page
  display.ssd1306_command(0x00);
  display.ssd1306_command(0x0f);  // Stop Page
  display.ssd1306_command(0x00);
  display.ssd1306_command(0xFF);
  display.ssd1306_command(0x2F);
  
  //display.startscrollleft(0x00, 0x0F);
  display.drawLine(127, 32, 127, 32 - drawHeight, WHITE);
  //display.stopscroll();
  display.display();
  
  
  //drawAxises();
  //display.display();  //needed before anything is displayed
  //display.clearDisplay(); //clear before new drawing
}

void drawAxises()  //separate to keep stuff neater. This controls ONLY drawing background!
{
  int count;
  display.setCursor(90, 0);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  display.drawLine(0, 0, 0, 32, WHITE);
  display.drawLine(128, 0, 128, 32, WHITE);
  
  for (count = 0; count < 40; count += 10)
  {
    display.drawLine(128, count, 123, count, WHITE);
    display.drawLine(0, count, 5, count, WHITE);
  }

  for (count = 10; count < 128; count += 10)
  {
    display.drawPixel(count, 0 , WHITE);
    display.drawPixel(count, 31 , WHITE);
  }
}

void imprimirPresion() {
  if(tiempos.imprimirPresion) return;
  tiempos.imprimirPresion = 10;
  
  int tmpInt1 = presion;                  // Get the integer (678).
  float tmpFrac = presion - tmpInt1;      // Get fraction (0.0123).
  int tmpInt2 = trunc(tmpFrac * 1000);  // Turn into integer (123).
  
  strcpy(buff_oled, "       ");
  lcd_out(6, 7);
  
  sprintf(buff_oled, "Presion: %d,%d", tmpInt1, tmpInt2);
  lcd_out(1, 5);

  display.display();  //needed before anything is displayed
  display.clearDisplay(); 
}
