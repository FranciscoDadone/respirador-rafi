#include <TimerOne.h>
#include <Wire.h>
#include "Adafruit_SSD1306.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <ListLib.h>

#define PIN_LED 4
#define PIN_ALERTA_PRESION 7

#define PIN_POTE A0
#define PIN_FLUJO A1

Adafruit_SSD1306 display(-1);
#define OLED_ADDR   0x3C

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

//TIEMPOS DE ON Y DE OFF EN MS
#define T_MS
//#define T_S

#define PARADO              0
#define CICLO               1
#define CICLO_UNICO         2
#define STOP_CICLO_UNICO    3
#define CONFIGURACION       9

// Con pulsadores BLANCOS

#define BTN_MENOS     10
#define BTN_MAS       12
#define BTN_MENU      11

//////////////////////////////
#define BTN_MENOS_PORT  !digitalRead(BTN_MENOS) //con botones blancos --> !
#define BTN_MAS_PORT    !digitalRead(BTN_MAS)
#define BTN_MENU_PORT   !digitalRead(BTN_MENU)

#define TIEMPO_RUIDO_PULSADOR 200u
#define TIEMPO_PANTALLA 100u
#define TMR_SEGUNDOS 1000
#define TMR_MS 100

#define T_ON  0
#define T_OFF 1

// Define para el sensor de presion
#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10
#define SEALEVELPRESSURE_HPA (1013.25)

// Variable para el sensor de presion
Adafruit_BME280 bme; // I2C

char buff_oled[30];
byte sensorArray[128];
uint16_t paso;
int estadoMaquina=0;

float presionLeida;
float presionRespiratoria;
uint16_t presionMaxima = 10;
float presionAtmosferica = 1005;

float flujo = 0;
float integralFlujo = 0;

struct Tiempos
{
  uint16_t led;
  uint8_t mostarDato;
  uint8_t graficar;
  uint16_t motorParado;
  uint8_t usuario;
  uint16_t leerPresion;
  uint16_t imprimirPresion;
} volatile tiempos;


ISR(TIMER0_COMPA_vect)
{
  if(tiempos.led) tiempos.led--;
  if(tiempos.mostarDato) tiempos.mostarDato--;
  if(tiempos.graficar) tiempos.graficar--; 
  if(tiempos.motorParado) tiempos.motorParado--;
  if(tiempos.usuario) tiempos.usuario--;
  if(tiempos.leerPresion) tiempos.leerPresion--;
  if(tiempos.imprimirPresion) tiempos.imprimirPresion--;
}

void confTimer0(){
  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR2A register to 0
  TCCR0B = 0;// same for TCCR2B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 2khz increments
  //OCR0A = 249;//124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  OCR0A = 175;
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for 64 prescaler
  TCCR0B |= (1 << CS02) | (0 << CS01) | (1 << CS00);
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);  
}

void confDisplay(){
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();

  display.clearDisplay();
  sprintf(buff_oled, "Init");
  lcd_out(1, 1);
  display.display();
}

void confSensor(){
  if (!bme.begin(0x76, &Wire)) {
    display.clearDisplay();
    sprintf(buff_oled, "Error en el sensor");
    lcd_out(1, 2);
    display.display();
    while(1);  
  }  
}

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_LED, OUTPUT);
  pinMode(BTN_MENOS, INPUT_PULLUP);
  pinMode(BTN_MAS, INPUT_PULLUP);
  pinMode(BTN_MENU, INPUT_PULLUP);
  pinMode(PIN_POTE, INPUT);

  pinMode(PIN_ALERTA_PRESION, OUTPUT);

  confTimer0();
  confDisplay();
  confSensor();
  calibrarSensor();

  Timer1.initialize(10000);
  Timer1.attachInterrupt(medirFlujo);
}


void medirFlujo(){
  flujo = analogRead(PIN_FLUJO);
  integralFlujo += flujo;
}

void calibrarSensor(){
  int muestras = 0;
  presionAtmosferica = 0;
  display.clearDisplay();
  while(muestras<5){
    tiempos.leerPresion = 50;
    sprintf(buff_oled, "Calibrando...");
    lcd_out(1, muestras);
    display.display();
  
    while(tiempos.leerPresion);
    float p = bme.readPressure();
    if(p>985){
      muestras++;
      presionAtmosferica += p;
    }
  }
  
  presionAtmosferica /= 500.0F;
}


void setVariables(){
  if(tiempos.usuario) return;
  tiempos.usuario = 10;
  float pote = analogRead(PIN_POTE); 

  presionMaxima = pote * 20.0 / 1023.0;
}

void loop()
{
  setVariables();
  graficar(); 
  leerPresion();

  if(presionRespiratoria>presionMaxima){
    digitalWrite(PIN_ALERTA_PRESION, LOW);
  } else {
    digitalWrite(PIN_ALERTA_PRESION, HIGH);
  }
}

void leerPresion(){
  if(tiempos.leerPresion) return;
  tiempos.leerPresion = 1;
  
  presionLeida = bme.readPressure() / 100.0F;
  presionRespiratoria = (presionLeida>presionAtmosferica) ? (presionLeida - presionAtmosferica) : 0;
}


void lcd_out(uint8_t col, uint8_t linea)
{
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(col * 10, linea * 9);
  display.print(buff_oled);
}

void graficar(){
  byte drawHeight;
  int count;
  
  if(tiempos.graficar) return;
  tiempos.graficar = 5;

  // Muestra el valor de la presion
  int tmpInt1 = presionRespiratoria;                  // Get the integer (678).
  float tmpFrac = presionRespiratoria - (float)tmpInt1;      // Get fraction (0.0123).
  int tmpInt2 = trunc(tmpFrac * 100);  // Turn into integer (123).
  sprintf(buff_oled, "Presion: %d,%.2d hPa ", tmpInt1, tmpInt2);
  lcd_out(1, 6);
  /////////////////////////////////

  
  drawHeight = map(presionRespiratoria, 0, 13, 0, 50);
  
  sensorArray[0] = drawHeight;
  for (count = 1; count <= 128; count++ )
    display.drawLine(128 - count, 50, 128 - count, 50 - sensorArray[count - 1], WHITE);
   
  for (count = 160; count >= 2; count--) // count down from 160 to 2
    sensorArray[count - 1] = sensorArray[count - 2];

  drawAxises();
  display.display();  //needed before anything is displayed
  display.clearDisplay(); //clear before new drawing
}

void drawAxises()  //separate to keep stuff neater. This controls ONLY drawing background!
{
  int count;
  display.setCursor(90, 0);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  display.drawLine(0, 0, 0, 50, WHITE);
  display.drawLine(128, 0, 128, 50, WHITE);
  
  for (count = 0; count < 40; count += 10)
  {
    display.drawLine(128, count, 123, count, WHITE);
    display.drawLine(0, count, 5, count, WHITE);
  }

  for (count = 10; count < 128; count += 10)
  {
    display.drawPixel(count, 0 , WHITE);
    display.drawPixel(count, 49 , WHITE);
  }

  // Presion maxima
  byte drawHeight = map(presionMaxima-presionAtmosferica, 0, 100, 0, 32 );
  for (count = 10; count < 128; count += 10)
  {
    display.drawPixel(count, drawHeight, WHITE);
  }
}
