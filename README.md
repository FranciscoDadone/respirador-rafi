# Flow Monitor
## Introduction
A ventilator provides advanced ventilatory support. It is a device that controls and regulates a patient's breathing completely automatically. It delivers a controlled flow of air or oxygen to the lungs and facilitates the circulation of air to prevent the buildup of carbon dioxide. A mechanical ventilator refers to any device or equipment that assists or controls the breathing process. This can include oxygen masks, continuous positive airway pressure (CPAP) devices, and other equipment that provides respiratory support without the need for intubation. Mechanical ventilators are more basic devices compared to respirators, but they became of great importance during times like the pandemic when respirators were in short supply. They assisted medical personnel in providing the essential respiratory support to patients.

## Idea
Given the considerations mentioned above, and considering economic, technological, and logistical factors, as well as the lack of availability, there has been a proposal to elevate the standard of a mechanical ventilator, making it closer in capabilities to a respirator. It is understood that it will never replace all the features of a respirator, but it is recognized that its improvement would be of great utility.

## Proposal
The creation of a portable airflow monitor was proposed, designed for easy integration into various equipment. This device aimed to provide support to medical personnel, allowing them to have precise control over the administration of air to the patient. The monitor was designed as an essential tool for assessing the airflow response during patient delivery.

## Challenges
To carry out the development of the monitor, we coordinated meetings with specialists who provided us with a detailed explanation of the functioning of a respirator, as seen from the medical perspective. This allowed us to understand which parameters we needed to monitor and how we should present them to medical personnel, including their units of measurement and the critical associated data.

## Development
Utilizing a "bag-valve" type mechanical ventilator, we created a device capable of emulating the manual actions of medical personnel, including the simulation of the necessary frequency, pressure, and speed for the device to operate autonomously. During development, physicians emphasized that this solution would not only serve as a substitute but also as a way to provide rest to those professionals who were rotating due to exhaustion caused by manual operation while awaiting the availability of a respirator. Once we obtained the air pumping process, we proceeded to create the airflow monitor. For this, we divided it into three areas:

### Hydraulic Area
This area was responsible for generating an accessory that allowed us to connect the differential pressure sensors and create a pressure drop to calculate the airflow.

### Electronic Area
In this section, we determined which sensors to use and how to condition them for reading through programming.

### Design and Prototyping Area
This part of the project involved positioning the elements and establishing the connection between the hydraulic part of the system and the electronics of the monitor.

## Results
The outcome of this project was the creation of a mechanical assistive device capable of providing consistent frequency, speed, and a continuous and steady flow of air.
